#!/bin/sh
# install antigen and vim-plug
rm -rf ./antigen
git clone https://github.com/zsh-users/antigen.git
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

cp tmux.conf ~/.tmux.conf
mkdir -p ~/.config/nvim
cp init.vim ~/.config/nvim/
cp tmux.conf ~/.tmux.conf
cp zshrc ~/.zshrc

call plug#begin('~/.local/share/nvim/plugged')

Plug 'scrooloose/nerdtree'
Plug 'junegunn/fzf'
Plug 'Valloric/YouCompleteMe' , { 'do': './install.py' }
Plug 'neomake/neomake'
Plug 'altercation/vim-colors-solarized'
Plug 'wincent/command-t', {
    \   'do': 'cd ruby/command-t && ruby extconf.rb && make'
    \ }
Plug 'terryma/vim-expand-region'
Plug 'majutsushi/tagbar'
Plug 'sheerun/vim-wombat-scheme'
Plug 'rust-lang/rust.vim'
Plug 'lifepillar/vim-solarized8'
Plug 'itchyny/lightline.vim'

call plug#end()

let g:lightline = {
      \ 'colorscheme': 'solarized',
      \ }

let g:ycm_rust_src_path = '/home/steffen/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/src'

let mapleader = "\<Space>"

"save file
nnoremap <Leader>p :w<CR>

" open file
nnoremap <Leader>o :CtrlP<CR>
" copy/paste from clipboard
vmap <Leader>y "+y
vmap <Leader>d "+d
nmap <Leader>p "+p
nmap <Leader>P "+P
vmap <Leader>p "+p
vmap <Leader>P "+P


"region selection
vmap v <Plug>(expand_region_expand)
vmap <C-v> <Plug>(expand_region_shrink)

" tagbar opening
nmap <F8> :TagbarToggle<CR>

" set background=dark
set termguicolors
colorscheme solarized8_dark_flat

syntax enable
set number
"set relativenumber
set autoread
filetype indent on

set expandtab
set tabstop=4
set shiftwidth=4
set autoindent
set copyindent

autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

set wildignore+=node_modules/**

" nerdtree
let g:NERDTreeMouseMode = 2                                 " Single-click to expand the directory, double-click to open the file
let g:NERDTreeShowHidden = 1                                " Show hidden files
nmap <F2> :NERDTreeToggle<CR>

" auto open and close NERDTree
autocmd vimenter * NERDTree
autocmd VimEnter * wincmd p
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" U" Use ctrl-[hjkl] to select the active split!
nmap <silent> <Leader>k :wincmd k<CR>
nmap <silent> <Leader>j :wincmd j<CR>
nmap <silent> <Leader>h :wincmd h<CR>
nmap <silent> <Leader>l :wincmd l<CR>

" open splits to bottom and right
set splitbelow
set splitright

set guicursor=
